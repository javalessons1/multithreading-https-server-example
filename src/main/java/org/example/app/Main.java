package org.example.app;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.app.handler.ApartmentHandler;
import org.example.app.handler.UserHandler;
import org.example.app.manager.AvitoManager;
import org.example.app.manager.UserManager;
import org.example.framework.http.HttpMethods;
import org.example.framework.http.Server;
import org.example.framework.middleware.AnonAuthMiddleware;
import org.example.framework.middleware.BasicAuthNMiddleware;
import org.example.framework.util.Maps;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import java.util.regex.Pattern;

import static java.lang.Thread.sleep;

@Slf4j
public class Main {
    public static void main(String[] args) {
        System.setProperty("javax.net.ssl.keyStore", "web-certs/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "passphrase");
        final Gson gson = new Gson();
        final Argon2PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();
        final UserManager userManager = new UserManager(passwordEncoder);
        final UserHandler userHandler = new UserHandler(gson, userManager);
        final AvitoManager manager = new AvitoManager();
        final ApartmentHandler apartmentHandler = new ApartmentHandler(gson, manager);
        final String apartmentId = "(?<apartmentId>[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})";
        final String getById = "^/apartments/" + apartmentId + "$";
        final String update = "^/apartments/" + apartmentId + "/update$";
        final String delete = "^/apartments/" + apartmentId + "/delete$";
        final Server server = Server.builder()
                .middleware(new BasicAuthNMiddleware(userManager))
                .middleware(new AnonAuthMiddleware())
                .routes(
                        Maps.of(
                                Pattern.compile("^/users$"), Maps.of(
                                        HttpMethods.POST, userHandler::register
                                ),
                                Pattern.compile("^/apartments$"), Maps.of(
                                        HttpMethods.GET, apartmentHandler::getAll,
                                        HttpMethods.POST, apartmentHandler::create
                                ),
                                Pattern.compile(update), Maps.of(
                                        HttpMethods.POST, apartmentHandler::update
                                ),
                                Pattern.compile(getById), Maps.of(
                                        HttpMethods.GET, apartmentHandler::getById
                                ),
                                Pattern.compile(delete), Maps.of(
                                        HttpMethods.POST, apartmentHandler::delete
                                )
                        )
                )
                .build();
        int port = 8443;
        try {
            server.start(port);
            sleep(100_000);
            server.stop();
        } catch (Exception e) {
            log.debug("Error in main function occurred!");
        }
    }
}
