package org.example.app.manager;

import lombok.extern.slf4j.Slf4j;
import org.example.app.domain.Apartment;
import org.example.app.dto.*;
import org.example.app.exception.ApartmentNotFoundException;
import org.example.app.exception.AuthorizationException;
import org.example.framework.auth.SecurityContext;
import org.example.framework.auth.principal.AnonymousPrincipal;

import java.security.Principal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
public class AvitoManager {

    private final List<Apartment> items = new ArrayList<>(100);
    public List<ApartmentRS> getAll() {
        synchronized (this) {
            return items.stream()
                    .map(o -> new ApartmentRS(
                            o.getId(),
                            o.getRoomAmount(),
                            o.getPrice(),
                            o.getSize(),
                            o.isWithLoggia(),
                            o.isWithBalcony(),
                            o.getFloor(),
                            o.getFloorsInHouse(),
                            o.getCreated()
                    ))
                    .collect(Collectors.toList());
        }
    }

    public ApartmentRS create(final ApartmentRQ requestDTO) throws AuthorizationException {
        final Principal principal = SecurityContext.getPrincipal();
        synchronized (this) {
            if (principal.getName().equals(AnonymousPrincipal.ANONYMOUS)) {
                throw new AuthorizationException("User must be authorized!");
            }
            final Apartment item = new Apartment(
                    UUID.randomUUID().toString(),
                    requestDTO.getRoomAmount(),
                    requestDTO.getPrice(),
                    requestDTO.getSize(),
                    requestDTO.isWithLoggia(),
                    requestDTO.isWithBalcony(),
                    requestDTO.getFloor(),
                    requestDTO.getFloorsInHouse(),
                    principal.getName(),
                    Instant.now().getEpochSecond()
            );
            items.add(item);

            return new ApartmentRS(
                    item.getId(),
                    item.getRoomAmount(),
                    item.getPrice(),
                    item.getSize(),
                    item.isWithLoggia(),
                    item.isWithBalcony(),
                    item.getFloor(),
                    item.getFloorsInHouse(),
                    item.getCreated()
            );
        }
    }

    public ApartmentRS update(final ApartmentRQ requestDTO, final String id)
            throws ApartmentNotFoundException, AuthorizationException {
        final Principal principal = SecurityContext.getPrincipal();

        synchronized (this) {
            final Apartment item = getById(id);
            final int index = getIndexById(item.getId());
            if (index == -1) {
                throw new ApartmentNotFoundException("Apartment with such id does not exist");
            }
            final Apartment updatedItem = new Apartment(
                id,
                requestDTO.getRoomAmount(),
                requestDTO.getPrice(),
                requestDTO.getSize(),
                requestDTO.isWithLoggia(),
                requestDTO.isWithBalcony(),
                requestDTO.getFloor(),
                requestDTO.getFloorsInHouse(),
                principal.getName(),
                item.getCreated()
            );
            log.debug("updated successfully. id: {}", id);

            removeById(id);
            items.add(updatedItem);
            return new ApartmentRS(
                    updatedItem.getId(),
                    updatedItem.getRoomAmount(),
                    updatedItem.getPrice(),
                    updatedItem.getSize(),
                    updatedItem.isWithLoggia(),
                    updatedItem.isWithBalcony(),
                    updatedItem.getFloor(),
                    updatedItem.getFloorsInHouse(),
                    updatedItem.getCreated()
            );
        }
    }


    public void removeById(String id) throws
            ApartmentNotFoundException, AuthorizationException {
        synchronized (this) {
            final Principal principal = SecurityContext.getPrincipal();
            if (!getById(id).getOwner().equals(principal.getName())) {
                throw new AuthorizationException("The user is not the owner!");
            }
            final int index = getIndexById(id);
            if (index == -1) {
                throw new ApartmentNotFoundException("Apartment was deleted or never existed");
            }
            items.remove(index);
            log.debug("Apartment with id {} was removed", id);
        }

    }

    public int getCount() {
        synchronized (this) {
            return items.size();
        }
    }

    private int getIndexById(String id) {
        synchronized (this) {
            for (int i = 0; i < items.size(); i++) {
                if (items.get(i).getId().equals(id)) {
                    return i;
                }
            }
            return -1;
        }
    }

    public Apartment getById(final String id) throws ApartmentNotFoundException {
        synchronized (this) {
            for (final Apartment item : items) {
                if (item.getId().equals(id)) {
                    log.debug("item is found!");
                    return item;
                }
            }
            throw new ApartmentNotFoundException("Apartment was deleted or never existed");
        }
    }

}
